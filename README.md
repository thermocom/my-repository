**Comment choisir un plombier**

On s'est tous rencontré avec un problème de plomberie dans la vie. Une fuite d'eau, un robinet cassé, un chauffe-eau déffaillant, une chaudière baclé. 
C'est pas toujour evident comment trouver un plombier. Ci-dessous on vous invite de consulter les élements à prendre en compte quand on cherche un plombier. 
---

Vérifiez les élements suivants pour identifier un plombier de confiance:

Entreprise enregistré, Capital social solide, Certifié PG et RGE, Qualifié au minimum CAP, Assurance de responsabilité civile, Assurance décennale, Avis positifs sur internet

Pour plus d'informations: https://thermocom.fr
---
